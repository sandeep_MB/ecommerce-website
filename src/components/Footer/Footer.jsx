import React, { Component } from "react";
import "./Footer.css";

class Footer extends React.Component {
  render() {
    return (
      <div className="footer">
        <div className="footer-brand">
          <h3>Swap Shop</h3>
          <p>Copyright @2014</p>
        </div>
        <div className="footer-product">
          <h4>Products</h4>
          <li>Pricing</li>
          <li>Request Access</li>
          <li>Partnership</li>
        </div>

        <div className="about">
          <h4>About Us</h4>
          <li>Contact Us</li>
          <li>Features</li>
          <li>Careers</li>
        </div>
        <div className="resources">
          <h4>Resources</h4>
          <li>Help Center</li>
          <li>Book a demo</li>
          <li>Blogs</li>
        </div>
        <div>
          <div>
            <h4>Get in touch</h4>
            <p>Questions or feedback?</p>
            <p>We love to hear from you</p>
          </div>
          <div className="social-media"></div>
        </div>
      </div>
    );
  }
}

export default Footer;
