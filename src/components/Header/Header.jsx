import React, { Component } from "react";
import { Link } from "react-router-dom";
import "./Header.css";

class Header extends React.Component {
  render() {
    return (
      <div className="header">
        <div className="branding">
          <h4>Swap Shop</h4>
        </div>
        <div className="navbar">
          <ul>
            <li>
              <Link className="link" to="/">
                Home
              </Link>
            </li>
            <li>About Us</li>
            <li>
              {/* <Link className="link" to="/products"> */}
              Products
              {/* </Link> */}
            </li>
            <li>Contact Us</li>
          </ul>
        </div>
        <div class="dropdown">
          <button class="dropbtn">Home</button>
          <div class="dropdown-content">
            {/* <Link className="link" to="/">
              Home
            </Link> */}
            <Link className="link" to="/">
              About
            </Link>
            <Link className="link" to="/products">
              Product
            </Link>
            <Link className="link" to="/">
              Contact US
            </Link>
          </div>
        </div>
        <div className="cart-container">
          <div>
            <i class="fas fa-cart-plus"></i>
          </div>
          <div>
            <i class="fas fa-user"></i>
          </div>
        </div>
      </div>
    );
  }
}

export default Header;
