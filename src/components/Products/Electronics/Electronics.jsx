import React from "react";

class Electronics extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      electronics: [],
      error: false,
    };
  }

  getElectronicsData() {
    fetch("https://fakestoreapi.com/products/category/electronics")
      .then((res) => res.json())
      .then((data) => {
        this.setState({ electronics: data, error: true });
      })
      .catch((err) => console.error(err));
  }

  componentDidMount() {
    this.getElectronicsData();
  }
  render() {
    const { electronics, error } = this.state;
    return (
      <div>
        {error ? (
          <div>
            {electronics.map((ele) => {
              return (
                <div>
                  <h3>Id:- {ele.id}</h3>
                  <h3>Title:- {ele.title}</h3>
                  <img src={ele.image} alt="product-image" width="60px" />
                  <h5>price:- {ele.price}</h5>
                  <h6>Description:- {ele.description}</h6>
                </div>
              );
            })}
          </div>
        ) : undefined}
      </div>
    );
  }
}

export default Electronics;
