import React, { Component } from "react";
import { findRenderedDOMComponentWithClass } from "react-dom/test-utils";

class MenClothing extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      mensClothing: [],
      error: false,
    };
  }
  getMenClothingData() {
    fetch("https://fakestoreapi.com/products/category/men's clothing")
      .then((res) => res.json())
      .then((data) => {
        this.setState({ mensClothing: data, error: true });
      })
      .catch((err) => {
        console.error(err);
      });
  }
  componentDidMount() {
    this.getMenClothingData();
  }

  render() {
    const { mensClothing, error } = this.state;
    return (
      <div>
        {error ? (
          <div className="mensClothing-container">
            {mensClothing.map((mens) => {
              return (
                <div>
                  <h3>{mens.id}</h3>
                  <h3>{mens.title}</h3>
                  <p>{mens.price}</p>
                  <p>{mens.description}</p>
                  <h6>{mens.category}</h6>
                  <img
                    src={mens.image}
                    width="100px"
                    alt="Mens Clothing Image"
                  />
                </div>
              );
            })}
          </div>
        ) : undefined}
      </div>
    );
  }
}

export default MenClothing;
