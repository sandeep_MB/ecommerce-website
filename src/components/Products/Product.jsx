import React, { Component } from "react";
import "./Product.css";

class Product extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      products: [],
      error: false,
    };
  }

  getProductData() {
    fetch("https://fakestoreapi.com/products")
      .then((res) => res.json())
      .then((data) => {
        this.setState({ products: data, error: true });
      })
      .catch((err) => {
        console.error(err);
      });
  }

  componentDidMount() {
    this.getProductData();
  }
  render() {
    const { products, error } = this.state;
    return (
      <div>
        {error ? (
          <div className="product-container">
            {products.map((product) => {
              return (
                <div className="product">
                  <img
                    className="image"
                    src={product.image}
                    alt="Product Image"
                  />
                  <h4 className="product-title">{product.title}</h4>
                  <p className="product-price">Price ${product.price}</p>
                  <button className="btn-description ">Add to Cart</button>
                </div>
              );
            })}
          </div>
        ) : undefined}
      </div>
    );
  }
}

export default Product;
