import React, { Component } from "react";

class WomenClothing extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      womensClothing: [],
      error: false,
    };
  }

  getWomensClothingData() {
    fetch("https://fakestoreapi.com/products/category/women's clothing")
      .then((res) => res.json())
      .then((data) => {
        this.setState({ womensClothing: data, error: true });
      })
      .catch((err) => {
        console.error(err);
      });
  }

  componentDidMount() {
    this.getWomensClothingData();
  }
  render() {
    const { womensClothing, error } = this.state;
    return (
      <div>
        {error ? (
          <div className="women-clothing-container">
            {womensClothing.map((women) => {
              return (
                <div>
                  <h3>{women.id}</h3>
                  <h4>{women.title}</h4>
                  <p>{women.price}</p>
                  <p>{women.description}</p>
                  <h4>{women.category}</h4>
                  <img
                    src={women.image}
                    width="100px"
                    alt="Womens Clothing Image"
                  />
                </div>
              );
            })}
          </div>
        ) : undefined}
        ;
      </div>
    );
  }
}

export default WomenClothing;
