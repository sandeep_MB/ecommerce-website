import React, { Component } from "react";

class Jewelery extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      jewelery: [],
      error: false,
    };
  }

  getJeweleryData() {
    fetch("https://fakestoreapi.com/products/category/jewelery")
      .then((res) => res.json())
      .then((data) => this.setState({ jewelery: data, error: true }))
      .catch((err) => console.error(err));
  }

  componentDidMount() {
    this.getJeweleryData();
  }
  render() {
    const { jewelery, error } = this.state;
    return (
      <div>
        {error ? (
          <div>
            {jewelery.map((jewel) => {
              return (
                <div>
                  <h3>Id:- {jewel.id}</h3>
                  <h4>Title:- {jewel.title}</h4>
                  <h5>Price:- {jewel.price}</h5>
                  <img src={jewel.img} alt="" />
                  <h5>Description:-</h5>
                  <p>{jewel.description}</p>
                </div>
              );
            })}
          </div>
        ) : undefined}
      </div>
    );
  }
}

export default Jewelery;
