import "./App.css";
import Header from "./components/Header/Header";
import Home from "./components/Home/Home";
import Footer from "./components/Footer/Footer";
import Electronics from "./components/Products/Electronics/Electronics";
import Jewelery from "./components/Products/Jewelery/Jewelery";
import MenClothing from "./components/Products/MenClothing/MenClothing";
import WomenClothing from "./components/Products/WomenClothing/WomenClothing";
import Product from "./components/Products/Product";
import { Routes, Route } from "react-router-dom";

function App() {
  return (
    <div>
      <Header />
      <Routes>
        {/* <Route path="/" element={<Home />} /> */}
        <Route path="/products" element={<Product />} />
        <Route path="/products/jewelery" element={<Jewelery />} />
        <Route path="/products/electronics" element={<Electronics />} />
        <Route path="/products/men" element={<MenClothing />} />
        <Route path="/products/women" element={<WomenClothing />} />
      </Routes>
      <Home />
      <Product />
      <Footer />
    </div>
  );
}

export default App;
